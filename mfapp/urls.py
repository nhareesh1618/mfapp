"""mfapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from app.views import fund_view,fund_create,data_create,fund_statics,data_headers,fund_statics_sync,stat,code_create

urlpatterns = [
    # path('', include('bootstrap4.urls')),
    
    path('',fund_view),
    path('code',code_create),
    path('create/',fund_create),
    path('admin/', admin.site.urls),
    path('data_create/',data_create,name='data_create'),
    path('data_view/<str:t>',fund_statics,name='data'),
    path('statics/<pk>/',data_headers,name='headers'),
    path('sync/',fund_statics_sync,name='sync'),
    path('stat/',stat,name='stat'),

]
