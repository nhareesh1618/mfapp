from django.db import models

# Create your models here.


class Code(models.Model):
	code = models.IntegerField()

class Fund(models.Model):
	name = models.CharField(max_length=100)
	code = models.IntegerField()



class Data(models.Model):
	fund = models.ForeignKey(Fund, on_delete=models.CASCADE)
	date = models.DateField()
	nav = models.DecimalField(max_digits=10, decimal_places=5)

	def __str__(self):
		return str(self.date)


class ThreeM(models.Model):
	fund = models.ForeignKey(Fund, on_delete=models.CASCADE)
	date = models.DateField()
	nav = models.DecimalField(max_digits=10, decimal_places=5,null=True)
	percentage = models.DecimalField(max_digits=10, decimal_places=5,null=True)
	period = models.CharField(max_length=15)

	def __str__(self):
		return self.fund.name