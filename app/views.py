from django.shortcuts import render
from django.http import HttpResponse
import requests as req
import datetime
from .models import Fund,Data,ThreeM,Code
from decimal import Decimal
import dateutil.relativedelta
# Create your views here.
from dateutil.relativedelta import relativedelta
from datetime import datetime
from django.db.models import Max


def code_create(request):
	
	mf_url = "https://api.mfapi.in/mf"
	r=req.get(mf_url)
	print(r)
	data = r.json()
	print(data)
	for i in data:
		obj, created = Code.objects.get_or_create(code=i['schemeCode'])			
	return HttpResponse("success")


def demo(request):
  
    # return response
    print("deno")
    return render(request, "demo.html")

def fund_create(request):
	for c in Code.objects.all():
		mf_url = "https://api.mfapi.in/mf/"
		r=req.get(mf_url+str(c.code))
		meta_data = r.json()['meta']
		obj, created = Fund.objects.get_or_create(name=meta_data['scheme_name'],code=meta_data['scheme_code'])
		print(meta_data['scheme_name'])
	return HttpResponse("success")

def data_create(request):
	for fund in Fund.objects.all().order_by('-id'):
		mf_url = "https://api.mfapi.in/mf/"
		code = fund.code
		print(code)
		r=req.get(mf_url+str(code))
		print(fund,"*****************fund name")
		# r=req.get('https://api.mfapi.in/mf/120823')
		meta_data = r.json()['data']
		for i in meta_data:
			actual_date = datetime.strptime(i['date'],'%d-%m-%Y').strftime('%Y-%m-%d')
			obj, created = Data.objects.get_or_create(fund=fund,date=actual_date,nav=i['nav'])			
		print("success")
	return HttpResponse("success")

	# d={}
	# for i in Data.objects.all().order_by('-date'):
	# 	previous= Data.objects.filter(date__lt=i.date).order_by('-date')
	# 	if previous.exists():
	# 		previous_nav = previous[0].nav
	# 		p = round(((Decimal(i.nav) - previous_nav)/previous_nav)*100,2)
	# 		d[i.date]=p
	# 	# print(Decimal(i.nav),"***",previous_nav)
	# 	# Data.objects.create()
	# print(d)
	# data = Data.objects.all().order_by('-date')


	# return render(request,"data_view.html",context={"data":data,"nav_dict":d})
	


def fund_view(request):
	fund = Fund.objects.all()
	print(fund)
	return render(request,"fund_view.html",context={"fund_data":fund})



def fund_statics(request,t):
	pk = request.GET.get('pk')
	fund = Fund.objects.get(pk=pk)
	current_date1=datetime.now().strftime("%Y-%m-%d")
	current_date = datetime.strptime(current_date1,"%Y-%m-%d")
	total_l=[]
	time_period_key = t[1:]
	time_period_value = t[0]
	while True:
		d={}
		if t == "3months":
			one_month_ago=current_date - relativedelta(months=3)
		elif t == "2months":
			one_month_ago=current_date - relativedelta(months=2)
		elif t == "1months":
			one_month_ago=current_date - relativedelta(months=1)
		elif t == "6months":
			one_month_ago=current_date - relativedelta(months=6)
		elif t == "1years":
			one_month_ago=current_date - relativedelta(years=1)
		elif t == "2years":
			one_month_ago=current_date - relativedelta(years=2)
		elif t == "3years":
			one_month_ago=current_date - relativedelta(years=3)
		elif t == "5years":
			one_month_ago=current_date - relativedelta(years=5)


		current_nav = Data.objects.filter(date__lt=current_date).order_by('-date').first()
		previous_nav = Data.objects.filter(date__lt=one_month_ago).order_by('-date').first()
		if previous_nav is not None and current_nav is not None:
			print(current_nav.date,"*****",current_nav.nav)
			print(previous_nav.date,"*****",previous_nav.nav)
			p = round(((Decimal(current_nav.nav) - previous_nav.nav)/previous_nav.nav)*100,2)
			d["percentage"]=p
			d["nav"]=current_nav.nav
			d["date"] = current_nav.date
			print(p)
			obj, created = ThreeM.objects.get_or_create(fund=fund,percentage=p,date=current_nav.date,nav=current_nav.nav,period=t)

			previous_nav = previous_nav.nav
			current_date = one_month_ago
			print("_______________")
		else:
			break
		total_l.append(d)
	print(total_l)
	return render(request,"data_view.html",context={"nav_dict":total_l})
	# return HttpResponse("success")


def data_headers(request,pk):
	return render(request,"data_headers.html",{"key":pk})




def fund_statics_sync(request):
	
	for fund in Fund.objects.all():
		
		periods_list = ["3months","2months","1months","6months","1years","2years","3years","5years"]
		total_l=[]
		for period in periods_list:
			current_date1=datetime.now().strftime("%Y-%m-%d")
			current_date = datetime.strptime(current_date1,"%Y-%m-%d")
			while True:
				d={}
				if period== "3months":
					one_month_ago=current_date - relativedelta(months=3)
				elif period == "2months":
					one_month_ago=current_date - relativedelta(months=2)
				elif period == "1months":
					one_month_ago=current_date - relativedelta(months=1)
				elif period == "6months":
					one_month_ago=current_date - relativedelta(months=6)
				elif period == "1years":
					one_month_ago=current_date - relativedelta(years=1)
				elif period == "2years":
					one_month_ago=current_date - relativedelta(years=2)
				elif period == "3years":
					one_month_ago=current_date - relativedelta(years=3)
				elif period == "5years":
					one_month_ago=current_date - relativedelta(years=5)
			
				current_nav = Data.objects.filter(date__lt=current_date,fund=fund).order_by('-date').first()
				previous_nav = Data.objects.filter(date__lt=one_month_ago,fund=fund).order_by('-date').first()
				if previous_nav is not None and current_nav is not None:
					print(current_nav.date,"*****",current_nav.nav)
					print(previous_nav.date,"*****",previous_nav.nav)
					print(fund.id)
					p = round(((Decimal(current_nav.nav) - previous_nav.nav)/previous_nav.nav)*100,2)
					d["percentage"]=p
					d["nav"]=current_nav.nav
					d["date"] = current_nav.date
					print(p)
					obj, created = ThreeM.objects.get_or_create(fund=fund,percentage=p,date=current_nav.date,nav=current_nav.nav,period=period)
					previous_nav = previous_nav.nav
					current_date = one_month_ago
					print("_______________")
				else:
					break
				total_l.append(d)
	# return render(request,"data_view.html",context={"nav_dict":total_l})
	return HttpResponse("success")

def stat(request):
	current_date1=datetime.now().strftime("%Y-%m-%d")
	current_date = datetime.strptime(current_date1,"%Y-%m-%d")
	fund =Fund.objects.all()
	final_list = []
	one_mlist = []
	two_mlist = []
	three_mlist = []
	six_mlist = []
	one_ylist = []
	two_ylist = []
	three_ylist = []
	five_ylist = []
	for f in fund:
		periods_list = ["3months","2months","1months","6months","1years","2years","3years","5years"]
		for p in periods_list:
			print("*****")
			d = {}
			threem=ThreeM.objects.filter(date__lt=current_date,fund=f,period=p).first()
			print(threem)
			if threem:
				d['fund_name'] = threem.fund.name
				d['date'] = threem.date
				d['nav'] = threem.nav
				d['percentage'] = round(threem.percentage,2)
				d['period'] = p
			if p == "1months":
				one_mlist.append(d)
			elif p == "2months":
				two_mlist.append(d)
			elif p == "3months":
				three_mlist.append(d)
			elif p == "6months":
				six_mlist.append(d)
			elif p == "1years":
				one_ylist.append(d)
			elif p == "2years":
				two_ylist.append(d)
			elif p == "3years":
				three_ylist.append(d)
			elif p == "5years":
				five_ylist.append(d)
			else:
				pass
			final_list.append(d)
	print(one_mlist)
	one_mlist=sorted(one_mlist, key = lambda i: i['percentage'],reverse=True)
	two_mlist=sorted(two_mlist, key = lambda i: i['percentage'],reverse=True)
	three_mlist=sorted(three_mlist, key = lambda i: i['percentage'],reverse=True)
	six_mlist=sorted(six_mlist, key = lambda i: i['percentage'],reverse=True)
	one_ylist=sorted(one_ylist, key = lambda i: i['percentage'],reverse=True)
	two_ylist=sorted(two_ylist, key = lambda i: i['percentage'],reverse=True)
	three_ylist=sorted(three_ylist, key = lambda i: i['percentage'],reverse=True)
	five_ylist=sorted(five_ylist, key = lambda i: i['percentage'],reverse=True)
	print(one_mlist)
	no_of_list = 3
	one_mlist=one_mlist[:no_of_list]
	two_mlist=two_mlist[:no_of_list]
	three_mlist=three_mlist[:no_of_list]
	six_mlist=six_mlist[:no_of_list]
	one_ylist=one_ylist[:no_of_list]
	two_ylist=two_ylist[:no_of_list]
	three_ylist=three_ylist[:no_of_list]
	five_ylist=five_ylist[:no_of_list]
	# max_percentage = max(th, key=lambda x:x['percentage'])
	context = {"final_list":final_list,"one_mlist":one_mlist,"two_mlist":two_mlist,
	"three_mlist":three_mlist,"six_mlist":six_mlist,"one_ylist":one_ylist,
	"two_ylist":two_ylist,"three_ylist":three_ylist,"five_ylist":five_ylist}

	return render(request,"stat_view.html",context=context)

























